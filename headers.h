#ifndef HEADERS_H
#define HEADERS_H
#include "libs/frames.h"
#include "libs/plugins.h"

unsigned short parseFrame(FrameListener * listener, fh_frame * frame);
void headers_init();
#endif