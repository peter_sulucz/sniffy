#ifndef COMMAND_LINE_H
#define COMMAND_LINE_H
#include "list.h"
typedef struct
{
	char raw[1024];
	char * prog;
	struct list arguments;
	int count;
} cmd_line;
typedef struct
{
	struct list_elem elem;
	char * spec;
	char * arg;
} cmd_argument;

cmd_line * parseline(char * line);
void killCmdLine(cmd_line * cmd);
#endif