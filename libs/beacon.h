#ifndef BEACON_H
#define BEACON_H
#include "frames.h"
typedef struct __attribute__((__packed__))
{
	fh_frame_control framecontrol;
	unsigned short duration;
	unsigned char DA[6];
	unsigned char SA[6];
	unsigned char bssid[6];
	unsigned short sequencecontrol;
	unsigned long timestamp;
	unsigned short interval;
	unsigned short capability;
	unsigned short ssidlen;
	unsigned char ssid[32];
} fh_beacon;

void parseBeacon(fh_wifi * wifi);

void copySSID(fh_wifi * wifi, char * buffer, int len);
#endif