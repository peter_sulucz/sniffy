#include "threadpool.h"
#include "stdlib.h"
#include "stdio.h"
#include "pthread.h"
#include "list.h"
#include "semaphore.h"

static __thread int THREADINDEX = -1;


/*
	Struct declarations
*/
struct thread_pool{
	struct thread_inst * threadList;
	int max;
	int count;
	struct list queue;
	pthread_mutex_t queueMutex;
	sem_t queue_sem;
	bool ending;
};

// Holds thread information
struct thread_inst
{
	pthread_t thread;
	struct thread_pool * pool;
	struct list queue;
	int index;
	pthread_mutex_t queueMutex;
	pthread_attr_t attr;
	
};

// state of a future
typedef enum _future_state
{
	WAITING = 1,
	RUNNING = 2,
	FINISHED = 4
} future_state;

// an actual future
struct future{
	fork_join_task_t task;
	void * data;
	void * ret;
	sem_t semaphore;
	struct thread_inst * thread;
	volatile future_state state;
	struct list_elem elem;
};

/*
	Function declarations
*/

static void * thread_exec(void *);
static bool future_remove_safe(struct future * ftr);

/*
	Remove a future frome its list safely
*/
static bool future_remove_safe(struct future * ftr)
{
	bool success = false;
	struct thread_inst * thread = ftr->thread;
	pthread_mutex_lock(&thread->queueMutex);
	success = ftr->state == WAITING;
	if(success)
	{
		list_remove(&ftr->elem);
		ftr->state = RUNNING;
	}
	pthread_mutex_unlock(&thread->queueMutex);
	return success;
}

static void init_thead_inst(struct thread_inst * t, int index, struct thread_pool* pool)
{
	t->pool = pool;
	list_init(&t->queue);
	t->index = index;
	pthread_attr_init(&t->attr);
	pthread_mutex_init(&t->queueMutex, (pthread_mutexattr_t *)NULL);
}

static void run_thread(struct thread_inst * t)
{
	pthread_create(&t->thread, &t->attr, thread_exec, t);
}

/* Create a new thread pool with no more than n threads. */
struct thread_pool * thread_pool_new(int nthreads)
{
	struct thread_pool * pool = (struct thread_pool *)malloc(sizeof(struct thread_pool));
	if(!pool)
		return NULL;
	//pthread_mutex_create();
	pool->max = nthreads;
	pool->count = 0;
	pool->ending = false;
	list_init(&pool->queue);
	pool->threadList = (struct thread_inst *)malloc(sizeof(struct thread_inst) * nthreads);
	int i = 0;
	sem_init(&pool->queue_sem, 0, 0);
	pthread_mutex_init(&pool->queueMutex, (pthread_mutexattr_t *)NULL);
	for(; i < nthreads; i++)
	{
		init_thead_inst(&pool->threadList[i], i, pool);
	}
	i=0;
	for(; i < nthreads; i++)
	{
		run_thread(&pool->threadList[i]);
	}
	return pool;
}

// check if the pool is ending
static bool is_ending(struct thread_pool * pool)
{
	pthread_mutex_lock(&pool->queueMutex);
	bool ending = pool->ending;
	pthread_mutex_unlock(&pool->queueMutex);
	return ending;
}

/* 
 * Shutdown this thread pool in an orderly fashion.  
 * Tasks that have been submitted but not executed may or
 * may not be executed.
 *
 * Deallocate the thread pool object before returning. 
 */
void thread_pool_shutdown_and_destroy(struct thread_pool *pool)
{
	int i = 0;
	// inform all threads that they are ending
	pthread_mutex_lock(&pool->queueMutex);
	pool->ending = true;
	pthread_mutex_unlock(&pool->queueMutex);
	// post to all semaphores to free all blocked threads
	for(i = 0; i < pool->max; i++)
	{
		sem_post(&pool->queue_sem);
	}
	// join each thread
	for(i = 0; i < pool->max; i++)
	{
		pthread_join(pool->threadList[i].thread, NULL);
	}
	// free the threadlist
	free(pool->threadList);
	// free the pool
	free(pool);
}


/* 
 * Submit a fork join task to the thread pool and return a
 * future.  The returned future can be used in future_get()
 * to obtain the result.
 * 'pool' - the pool to which to submit
 * 'task' - the task to be submitted.
 * 'data' - data to be passed to the task's function
 *
 * Returns a future representing this computation.
 */
struct future * thread_pool_submit(
        struct thread_pool *pool, 
        fork_join_task_t task, 
        void * data)
{
	// allocate the future
	struct future * fut = (struct future *)malloc(sizeof(struct future));
	// if that failed, return null...
	if(!fut)
		return NULL;
	// set the task
	fut->task = task;
	// set the data
	fut->data = data;
	// initialize the semphore to 0
	sem_init(&fut->semaphore, 0, 0);
	// initialize the return value to null
	fut->ret = NULL;
	// initialize the state to waiting
	fut->state = WAITING;
	// if the threadindex is greater than or equal to zero, we are in one of the workers
	if(THREADINDEX >= 0)
	{
		pthread_mutex_lock(&pool->threadList[THREADINDEX].queueMutex);
        fut->thread = &pool->threadList[THREADINDEX];
		list_push_front(&pool->threadList[THREADINDEX].queue, &fut->elem);
		pthread_mutex_unlock(&pool->threadList[THREADINDEX].queueMutex);
	}
	else
	{
		// we are in the main thread, add to global list
		pthread_mutex_lock(&pool->queueMutex);
		list_push_front(&pool->queue, &fut->elem);
		pthread_mutex_unlock(&pool->queueMutex);
	}
	sem_post(&pool->queue_sem);
	return fut;
}

/*
	Get a future from a list element, and set its state to running, also set its thread
*/
static struct future * get_future_from_elem(struct list_elem * elem, struct thread_inst * thread)
{
	struct future * f = list_entry(elem, struct future, elem);
	f->state = RUNNING;
	f->thread = thread;
	return f;
}

static struct future * get_next_future(struct thread_inst * tinfo)
{
	struct future * fut = NULL;


	pthread_mutex_lock(&tinfo->queueMutex);
	if(!list_empty(&tinfo->queue))
	{
		fut = get_future_from_elem(list_pop_front(&tinfo->queue), tinfo);
		pthread_mutex_unlock(&tinfo->queueMutex);
		return fut;
	}
	pthread_mutex_unlock(&tinfo->queueMutex);


	pthread_mutex_lock(&tinfo->pool->queueMutex);
	if(!list_empty(&tinfo->pool->queue))
	{
		fut = get_future_from_elem(list_pop_front(&tinfo->pool->queue), tinfo);
		pthread_mutex_unlock(&tinfo->pool->queueMutex);
		return fut;
	}
	pthread_mutex_unlock(&tinfo->pool->queueMutex);


	int i = 1;
	int ind;
	for(;i < tinfo->pool->max; i++)
	{
		ind = (THREADINDEX + i) % tinfo->pool->max;

		if(ind < 0)
			fprintf(stderr, "%d\n", ind);

		pthread_mutex_lock(&tinfo->pool->threadList[ind].queueMutex);
		if(!list_empty(&tinfo->pool->threadList[ind].queue))
		{
			fut = get_future_from_elem(list_pop_back(&tinfo->pool->threadList[ind].queue), tinfo);
			pthread_mutex_unlock(&tinfo->pool->threadList[ind].queueMutex);
			return fut;
		}
		pthread_mutex_unlock(&tinfo->pool->threadList[ind].queueMutex);
	}
	return fut;
}

/*
	Execute a future
*/
static void run_future(struct future * ftr)
{
	ftr->state = RUNNING;
	ftr->ret = ftr->task(ftr->thread->pool, ftr->data);
	ftr->state = FINISHED;
}

/*
	Thread execution loop
*/
static void * thread_exec(void * threadInfo)
{
	// get the thread instance pointer
	struct thread_inst * tinfo = threadInfo;
	// set threadindex equal to the current index
	THREADINDEX = tinfo->index;
	struct future * ftr = NULL;
	do
	{
		// wait until a thread shows up somewhere
		sem_wait(&tinfo->pool->queue_sem);

		// get the next future
		ftr = get_next_future(tinfo);
		
		// if nothing went wrong, and that future is set, run it
		if(ftr)
		{
			run_future(ftr);
           	sem_post(&ftr->semaphore);
		}
	} while(!is_ending(tinfo->pool));
	return NULL;
}

/* Make sure that the thread pool has completed the execution
 * of the fork join task this future represents.
 *
 * Returns the value returned by this task.
 */
void * future_get(struct future * ftr)
{

	void * ret = NULL;

	if(THREADINDEX >=0 && future_remove_safe(ftr))
	{
		run_future(ftr);
	}
	else
		sem_wait(&ftr->semaphore);
	ret = ftr->ret;
	return ret;
}

/* Deallocate this future.  Must be called after future_get() */
void future_free(struct future * ftr)
{
	free(ftr);
}
