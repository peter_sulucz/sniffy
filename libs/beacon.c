#include "beacon.h"
#include <stddef.h>
#include <stdio.h>
#include <netinet/in.h>

void parseBeacon(fh_wifi * wifi)
{
	fh_beacon * beacon = (fh_beacon * )wifi;
	int ssidlen = ntohs(beacon->ssidlen);
	fprintf(stdout, "SSID: ");
	for(int i = 0; i < ssidlen; i++)
		fprintf(stdout, "%c", beacon->ssid[i]);
	fprintf(stdout, "\n");
}

void copySSID(fh_wifi * wifi, char * buffer, int len)
{
	fh_beacon * beacon = (fh_beacon * )wifi;
	int ssidlen = ntohs(beacon->ssidlen);
	for(int i = 0; i < ssidlen && i < len-1; i++)
	{
		buffer[i] = beacon->ssid[i];
	}
	buffer[ssidlen > len-1 ? len-1 : ssidlen] = 0;
}