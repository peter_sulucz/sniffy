#ifndef HELPERS_H
#define HELPERS_H
#include "frames.h"


char macEqual(char * mac1, char * mac2);

void writeFrame(int socket, char * frame, int length);

void writeProbe(int socket, int channel);

char * writeRadioTap(char * buffer, int channel);

unsigned int calculatefcs(unsigned char * buffer, unsigned int length);

void printMAC(char * mac);

unsigned long timestamp();

unsigned short getChannelFrequency(int channel);

void set_channel(FrameListener * listener, int channel);

#endif