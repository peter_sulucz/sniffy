#include "fake_network.h"
#include "beacon.h"
#include <string.h>
#include <unistd.h>

static void verbose(cmd_line * cmd);

static FrameListener * frameListener;
static void init(FrameListener * listener);
static void tick();
static struct list fakenets;

static void process(char * buffer, int length);
static char filter(char type, char subtype);

struct fakenet{
	struct list_elem elem;
	char mac[6];
	char ssid[32];
	int beaclen;
	int probelen;
	char beacon[200];
	char proberesponse[200];
	int channel;
};

sniff_plugin * FAKE_getlist()
{
	sniff_plugin * plugin = (sniff_plugin *)malloc(sizeof(sniff_plugin));
	snprintf(plugin->name, 256, "fake");
	snprintf(plugin->description, 1024, "Send beacons for a fake network");
	plugin->filter = filter;
	plugin->init = init;
	plugin->process = process;
	plugin->verbose = verbose;
	plugin->tick = tick;
	return plugin;
}

static void process(char * buffer, int length)
{
	fh_radiotap * radio = (fh_radiotap *) buffer;
	fh_management * manage = (fh_management *)endOfRadio(radio);

	struct list_elem * e = list_begin(&fakenets);
	for(; e != list_end(&fakenets); e = list_next(e))
	{
		struct fakenet * net = list_entry(e, struct fakenet, elem);
		
		if(macEqual((char *)&manage->DA, (char *)&net->mac) || macEqual((char *)&manage->DA, "\xFF\xFF\xFF\xFF\xFF\xFF"))
		{
			fh_beacon * respmanage = (fh_beacon *)endOfRadio((fh_radiotap *)&net->proberesponse);
			respmanage->timestamp = timestamp();
			memcpy(respmanage->DA, manage->SA, 6);
			memcpy(respmanage->SA, net->mac, 6);
			memcpy(respmanage->bssid, net->mac, 6);
			unsigned int * fcs = (unsigned int *)(net->proberesponse + net->probelen) -1;
			*fcs = calculatefcs((unsigned char *)respmanage, (unsigned long)fcs - (unsigned long)respmanage);
			set_channel(frameListener, net->channel);
			write(frameListener->socket, net->proberesponse, net->probelen);
			//break;
		}
	}
}

static char filter(char type, char subtype)
{
	return type == 0 && subtype == 4;
}

static void verbose(cmd_line * cmd)
{
	int channel = rand() % 13 + 1;
	fprintf(stderr, "Format: -f(add|remove|list) -m <bssid> -s <ssid>\n");
	struct fakenet * net = (struct fakenet *)malloc(sizeof(struct fakenet));
	int ssidlen;
	struct list_elem * e = list_begin(&cmd->arguments);
	for(; e != list_end(&cmd->arguments); e = list_next(e))
	{
		cmd_argument * arg = list_entry(e, cmd_argument, elem);
		if(arg->spec[1] == 's')
		{
			ssidlen = strlen(arg->arg);
			memcpy(&net->ssid, arg->arg, ssidlen);
			continue;
		}
		else if(arg->spec[1] == 'c')
		{
			channel = atoi(arg->arg);
			continue;
		}
	}
	net->channel = channel;
	fprintf(stderr, "SSID %s Channel: %u\n", net->ssid, channel);
	fh_beacon * beac = (fh_beacon *)writeRadioTap(net->beacon, channel);
	unsigned short * fc = (unsigned short *)beac;
	*fc = 0x80;
	beac->duration = 0;
	for(int i = 0; i < 6; i++)
		beac->DA[i] = 0xFF;
	for(char i= 0; i < 6; i++)
		net->mac[i] = rand() % 0xFF;
	memcpy(&beac->SA, &net->mac, 6);
	memcpy(&beac->bssid, &net->mac, 6);
	beac->sequencecontrol = 0;
	beac->timestamp = timestamp();
	beac->interval = 0;
	beac->capability = 0;
	char * params = (char *)&beac->timestamp;
	params += 8;
	memcpy(params, "\x00\x01\x31\x04", 4);
	params += 4;
	memcpy(params, "\x00", 1);
	params += 1;
	*params = ssidlen;
	params += 1;
	memcpy(params, net->ssid, ssidlen);
	params += ssidlen;
	memcpy(params, "\x01\x08\x82\x84\x8b\x96\x0c\x12\x18\x24", 10);
	params += 10;
	memcpy(params, "\x03\x01", 2);
	params += 2;
	// channel
	*params = channel;
	params += 1;
	memcpy(params, "\x2a\x01\x04", 3);
	params += 3;
	memcpy(params, "\x32\x04\x0c\x18\x30\x60", 6);
	params += 6;
	unsigned int * fcs = (unsigned int *)params;
	*fcs = calculatefcs((unsigned char *)beac, (unsigned long)params - (unsigned long)beac);
	//memcpy(params, "\xe9\xcc\x47\x44", 4);
	params += 4;
	net->beaclen = (unsigned int)((unsigned long)params - (unsigned long)&net->beacon);
	net->probelen = net->beaclen;

	memcpy(net->proberesponse, net->beacon, net->beaclen);
	beac = (fh_beacon *)writeRadioTap(net->proberesponse, 4);
	fc = (unsigned short *)beac;
	*fc = 0x50;

	list_push_back(&fakenets, &net->elem);
}

static void init(FrameListener * listener)
{
	frameListener = listener;
	list_init(&fakenets);
}

static void tick()
{
	struct list_elem * e = list_begin(&fakenets);
	for(; e != list_end(&fakenets); e = list_next(e))
	{
		struct fakenet * net = list_entry(e, struct fakenet, elem);
		fh_beacon * beac = (fh_beacon *)endOfRadio((fh_radiotap * )net->beacon);
		beac->timestamp = timestamp();
		unsigned int * fcs = (unsigned int *)(net->beacon + net->beaclen) -1;
		*fcs = calculatefcs((unsigned char *)beac, (unsigned long)fcs - (unsigned long)beac);
		set_channel(frameListener, net->channel);
		write(frameListener->socket, net->beacon, net->beaclen);
	}
}