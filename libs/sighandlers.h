#ifndef SIGHANDLERS_H
#define SIGHANDLERS_H
#include "frames.h"
#include <signal.h>
#include <unistd.h>

typedef void (*sighandler)(int, siginfo_t *, void *);
void initSighandlers(FrameListener * listener);
void signalblock(int signal);
void signalunblock(int signal);
#endif