#include "probe_networks.h"
#include "helpers.h"
#include <stdlib.h>
#include <stdio.h>

static void verbose(cmd_line * cmd);


static FrameListener * frameListener;
static void init(FrameListener * listener);


sniff_plugin * PROBE_getlist()
{
	sniff_plugin * plugin = (sniff_plugin *)malloc(sizeof(sniff_plugin));
	snprintf(plugin->name, 256, "probe");
	snprintf(plugin->description, 1024, "Probe all of the networks within range of this computer.");
	plugin->filter = NULL;
	plugin->init = init;
	plugin->process = NULL;
	plugin->verbose = verbose;
	plugin->tick = NULL;
	return plugin;
}

static void verbose(cmd_line * cmd)
{
	for(int i = 0; i< 100; i++)
		writeProbe(frameListener->socket, i % 13 + 1);
	fprintf(stderr, "Writing 100 probes\n");
}

static void init(FrameListener * listener)
{
	frameListener = listener;
}