#ifndef FRAMES_H
#define FRAMES_H
#include "list.h"
#include "pthread.h"

typedef struct __attribute__((__packed__))
{
	unsigned char dest[6];
	unsigned char source[6];
	unsigned short protocol;
} fh_ether;

typedef struct __attribute__((__packed__))
{
	unsigned int version:2;
	unsigned int type:2;
	unsigned int subtype: 4;
	unsigned int toDS:1;
	unsigned int fromDS:1;
	unsigned int moreFrag:1;
	unsigned int retry:1;
	unsigned int power:1;
	unsigned int moreData:1;
	unsigned int wep:1;
	unsigned int order:1;
} fh_frame_control;

typedef struct __attribute__((__packed__))
{
	fh_frame_control framecontrol;
	unsigned short duration;
	unsigned char addr1[6];
	unsigned char addr2[6];
	unsigned char addr3[6];
	unsigned short control;
	unsigned char addr4[6];
} fh_wifi;

typedef struct __attribute__((__packed__))
{
	fh_frame_control framecontrol;
	unsigned short duration;
	unsigned char DA[6];
	unsigned char SA[6];
	unsigned char BSSID[6];
	unsigned short sequence_control;
} fh_management;

typedef struct __attribute__((__packed__))
{
	unsigned char version;
	unsigned char pad;
	unsigned short length;
	unsigned int present;
	unsigned long tsft;
	unsigned char flags;
	unsigned char rate;
	struct 
	{
		unsigned short freq;
		unsigned short flags;
	} channel;
	struct
	{
		unsigned char set;
		unsigned char pattern;
	} fhss;
	struct
	{
		char strength;
		char noise;
	} sig;
	unsigned short lock;
	struct
	{
		unsigned short value;
		unsigned short db;
		char dbm;
	} tx;
	struct
	{
		unsigned char index;
		unsigned char sig;
		unsigned char noise;
	} antenna;
	unsigned short RX_flags;
	struct
	{
		unsigned char known;
		unsigned char flags;
		unsigned char value;
	} mcs;
	struct {
		unsigned int ref;
		unsigned short flags;
		unsigned char crc;
		unsigned char reserved;
	} mpdu;
	struct{
		unsigned short known;
		unsigned char flags;
		unsigned char bandwidth;
		unsigned char mcs_nss[4];
		unsigned char coding;
		unsigned char group_id;
		unsigned short partial_aid;
	} vht;
	
} fh_radiotap;

typedef struct 
{
	union{
		unsigned char buffer[11000];
		struct 
		{
			union{
				fh_ether ethernet_header;
				fh_wifi wifi_header;
			};
			
		} frame;
	};
} fh_frame;

typedef struct
{
	int socket;
	int networkDevice;
	struct list activeCommands;
	pthread_mutex_t listmutex;
	struct thread_pool * threadpool;
	char running;
	char * deviceName;
} FrameListener;


fh_frame * INIT_Frame();
void KILL_Frame(fh_frame * frame);

fh_radiotap * parseRadiotap(fh_radiotap * buf);

fh_wifi * endOfRadio(fh_radiotap * radio);
#endif