#include "datastructures.h"

struct list network_list;
pthread_mutex_t network_list_mutex;

struct list station_list;
pthread_mutex_t station_list_mutex;

void init_datastructures()
{
	list_init(&network_list);
	pthread_mutex_init(&network_list_mutex, (pthread_mutexattr_t *)NULL);

	list_init(&station_list);
	pthread_mutex_init(&station_list_mutex, (pthread_mutexattr_t *)NULL);
}