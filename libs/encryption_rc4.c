#include "encyption_rc4.h"

static void swap(int i, int j, unsigned int * array)
{
	unsigned int temp = array[i];
	array[i] = array[j];
	array[j] = temp;
}

static void initSbox(RC4 * rc4, char * key, int len)
{
	int j = 0;
	for(int i = 0; i < SBOX_LENGTH; i++)
	{
		rc4->sbox[i] = i;
	}

	for(int i = 0; i < SBOX_LENGTH; i++)
	{
		j = (j+rc4->sbox[i] + key[i%len]) % SBOX_LENGTH;
		swap(i, j, rc4->sbox);
	}
}

RC4 * INIT_rc4(char * key)
{
	RC4 * rc4 = (RC4 *)malloc(sizeof(RC4));
	int len = strlen(key);
	len = len > SBOX_LENGTH ? SBOX_LENGTH : len;
	memcpy(&rc4->key, key, len);
	initSbox(rc4, key, len);
	return rc4;
}

void KILL_rc4(RC4 * rc4)
{
	free(rc4);
}