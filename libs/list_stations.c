#include "list_stations.h"
#include "frames.h"
#include "datastructures.h"

static void init(FrameListener * listener);
static void process(char * buffer, int length);
static char filter(char type, char subtype);
static void verbose(cmd_line * cmd);

struct station
{
	struct list_elem elem;
	char bssid[6];
	char mac[6];
};

static FrameListener * frameListener;

sniff_plugin * STATION_getlist()
{
	sniff_plugin * plugin = (sniff_plugin *)malloc(sizeof(sniff_plugin));
	snprintf(plugin->name, 256, "stations");
	snprintf(plugin->description, 1024, "List all of the networks within range of this computer.");
	plugin->filter = filter;
	plugin->init = init;
	plugin->process = process;
	plugin->verbose = verbose;
	plugin->tick = NULL;
	return plugin;
}

static char filter(char type, char subtype)
{
	return type == 0 && (subtype == 5);
}

static void init(FrameListener * listener)
{
	frameListener = listener;
}

static void verbose(cmd_line * cmd)
{
	pthread_mutex_lock(&station_list_mutex);
	struct list_elem * e = list_begin(&station_list);
	for(; e != list_end(&station_list); e = list_next(e))
	{
		struct station * stat = list_entry(e, struct station, elem);
		fprintf(stdout, "BSSID: ");
		printMAC(stat->bssid);
		fprintf(stdout, " STATION: ");
		printMAC(stat->mac);
		fprintf(stdout, "\n");
	}
	pthread_mutex_unlock(&station_list_mutex);
}

static void process(char * buffer, int length)
{
	fh_radiotap * radio = (fh_radiotap *) buffer;
	fh_management * manage = (fh_management *)endOfRadio(radio);

	pthread_mutex_lock(&station_list_mutex);
	struct list_elem * e = list_begin(&station_list);
	for(; e != list_end(&station_list); e = list_next(e))
	{
		struct station * stat = list_entry(e, struct station, elem);
		if(macEqual(stat->mac, manage->DA))
		{
			goto end;
		}
	}

	fh_radiotap * newtap = parseRadiotap(radio);
	struct station * stat = (struct station *)malloc(sizeof(struct station));
	for(int i = 0; i < 6; i++)
	{
		stat->bssid[i] = manage->BSSID[i];
	}
	for(int i = 0; i < 6; i++)
	{
		stat->mac[i] = manage->DA[i];
	}
	list_push_back(&station_list, &stat->elem);
	
	//fprintf(stdout, "Radio: %x\nFlags: %hhx\n Rate: %hhx\nCFreq: %hx Cflags: %hx\n", newtap->present, newtap->flags, newtap->rate, newtap->channel.freq, newtap->channel.flags);
	end:
	pthread_mutex_unlock(&station_list_mutex);
}