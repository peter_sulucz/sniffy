#include "command_line.h"
#include <string.h>
#include <stdlib.h>
cmd_line * parseline(char * line)
{
	cmd_line * cmd = (cmd_line *)malloc(sizeof(cmd_line));
	strcpy(cmd->raw, line);
	int len = strlen(cmd->raw);
	if(cmd->raw[len-1] == '\n')
		cmd->raw[len-1] = 0;
	list_init(&cmd->arguments);
	cmd->count = 0;
	cmd->prog = strtok(cmd->raw, " ");
	char * arg1;
	char * arg2;
	arg1 = strtok(NULL, " ");
	arg2 = strtok(NULL, " ");
	while(arg1 != NULL && arg2 != NULL)
	{
		cmd_argument * arg = (cmd_argument *)malloc(sizeof(cmd_argument));
		arg->spec = arg1;
		arg->arg = arg2;
		list_push_back(&cmd->arguments, &arg->elem);
		arg1 = strtok(NULL, " ");
		arg2 = strtok(NULL, " ");
		cmd->count++;
	}
	return cmd;
}

void killCmdLine(cmd_line * cmd)
{
	while(!list_empty(&cmd->arguments))
	{
		free(list_pop_front(&cmd->arguments));
	}
	free(cmd);
}