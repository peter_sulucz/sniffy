#include "list_networks.h"
#include "beacon.h"
#include "frames.h"
#include "list.h"
#include "pthread.h"
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include "helpers.h"
#include "datastructures.h"

static void init(FrameListener * listener);
static void process(char * buffer, int length);
static char filter(char type, char subtype);
static void verbose(cmd_line * cmd);
static void tick();

static FrameListener * frameListener;

struct beaconelem{
	struct list_elem elem;
	char ssid[100];
	char da[6];
	char sa[6];
	char bssid[6];
	int count;
};

sniff_plugin * LIST_getlist()
{
	sniff_plugin * plugin = (sniff_plugin *)malloc(sizeof(sniff_plugin));
	snprintf(plugin->name, 256, "list");
	snprintf(plugin->description, 1024, "List all of the networks within range of this computer.");
	plugin->filter = filter;
	plugin->init = init;
	plugin->process = process;
	plugin->verbose = verbose;
	plugin->tick = tick;
	return plugin;
}

static void tick()
{
	static int channel = 0;
	static int diff = 0;
	if(diff == 10)
	{
		diff =0;
		channel %= 13;
		channel++;
		
		//set_channel(frameListener, channel);
	};
	diff++;
	
}

static char filter(char type, char subtype)
{
	return type == 0 && (subtype == 8 || subtype == 5);
}

static void init(FrameListener * listener)
{
	frameListener = listener;
	//pthread_mutex_init(&list_mutex, (pthread_mutexattr_t *)NULL);
	//list_init(&network_list);
	//for(int i = 0; i< 100; i++)
	//	writeProbe(listener->socket);
}

static void verbose(cmd_line * cmd)
{
	pthread_mutex_lock(&network_list_mutex);
	struct list_elem * e = list_begin(&network_list);
	for(; e != list_end(&network_list); e = list_next(e))
	{
		struct beaconelem * belm = list_entry(e, struct beaconelem, elem);
		fprintf(stdout, "BSSID: ");
		printMAC(belm->bssid);
		fprintf(stdout, " SSID: %s\n", &belm->ssid);
	}
	pthread_mutex_unlock(&network_list_mutex);
}

static void process(char * buffer, int length)
{
	fh_radiotap * radio = (fh_radiotap *) buffer;
	fh_wifi * wifi = endOfRadio(radio);
	//if(wifi->framecontrol.subtype == 5)
	//	fprintf(stdout, "PROBE\n");
	//parseBeacon(wifi);
	char ssid[100];
	copySSID(wifi, ssid, 100);
	pthread_mutex_lock(&network_list_mutex);
	struct list_elem * e = list_begin(&network_list);
	for(; e != list_end(&network_list); e = list_next(e))
	{
		struct beaconelem * belm = list_entry(e, struct beaconelem, elem);
		if(macEqual(belm->bssid, wifi->addr3))
		{
			belm->count++;
			goto end;
		}
	}
	fh_radiotap * newtap = parseRadiotap(radio);
	struct beaconelem * beac = (struct beaconelem *)malloc(sizeof(struct beaconelem));
	strcpy(beac->ssid, ssid);
	for(int i = 0; i < 18; i++)
	{
		beac->da[i] = wifi->addr1[i];
	}
	list_push_back(&network_list, &beac->elem);
	
	//fprintf(stdout, "Radio: %x\nFlags: %hhx\n Rate: %hhx\nCFreq: %hx Cflags: %hx\n", newtap->present, newtap->flags, newtap->rate, newtap->channel.freq, newtap->channel.flags);
	end:
	pthread_mutex_unlock(&network_list_mutex);
}