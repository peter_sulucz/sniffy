#include "frames.h"
#include <stdlib.h>
#include <netinet/in.h>
#include <stdio.h>

fh_frame * INIT_Frame()
{
	fh_frame * frame = (fh_frame *)malloc(sizeof(fh_frame));
	return frame;
}
void KILL_Frame(fh_frame * frame)
{
	free(frame);
}

inline fh_wifi * endOfRadio(fh_radiotap * radio)
{
	return (fh_wifi *)((char *)radio + radio->length);
}

fh_radiotap * parseRadiotap(fh_radiotap * buf)
{
	fh_radiotap * rdio = (fh_radiotap *)malloc(sizeof(fh_radiotap));
	rdio->version = buf->version;
	rdio->pad = buf->pad;
	rdio->length = buf->length;
	rdio->present = buf->present;
	unsigned int present = rdio->present;
	char * begin = (char *)&buf->tsft;
	/*
	for(int i = 0; i < sizeof(fh_radiotap); i++)
	{
		fprintf(stderr, "%u:%hhi ",i, begin[i]);
	}
	fprintf(stderr, "%u\n", present);
	*/
	if(present & 1)
	{
		rdio->tsft = (unsigned long)*begin;
		begin += 8;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->flags = (unsigned char)*begin;
		begin += 1;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->rate = *begin;
		begin += 1;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->channel.freq = ntohs(*begin);
		begin += 2;
		rdio->channel.flags = ntohl(*begin);
		begin += 2;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->fhss.set = (unsigned char)*begin;
		begin += 1;
		rdio->fhss.pattern = (unsigned char)*begin;
		begin += 1;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->antenna.sig = (unsigned char)*begin;
		begin += 1;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->antenna.noise = (unsigned char)*begin;
		begin += 1;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->lock = (unsigned short)*begin;
		begin += 2;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->tx.value = (unsigned short)*begin;
		begin += 2;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->tx.db = (unsigned short)*begin;
		begin += 2;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->tx.dbm = (char)*begin;
		begin += 1;
	}
	present >>=1;
	if(present & 1)
	{
		rdio->antenna.index = (unsigned char)*begin;
		begin += 1;
	}
	present >>=1;
	return rdio;
}