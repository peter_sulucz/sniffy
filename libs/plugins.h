#ifndef PLUGINS_H
#define PLUGINS_H
#include <stdlib.h>
#include <stdio.h>
#include "list.h"
#include "frames.h"
#include "command_line.h"
#include "helpers.h"

extern struct list plugins_list; 

typedef enum{
	Management = 0x1,
	Control = 0x2,
	Data = 0x4
} frame_type;

typedef struct {
	struct list_elem elem;
	char name[256];
	char description[1024];
	char (* filter)(char type, char subtype);
	void (* init)(FrameListener * listener);
	void (* process)(char * buffer, int length);
	void (* verbose)(cmd_line * cmd);
	void (* tick)();
} sniff_plugin;

void initPlugins(FrameListener * listener);

void loadPlugins(char * directory);

sniff_plugin * findPlugin(char * name);

void printplugins();

#endif