#include "plugins.h"
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <dlfcn.h>
#include "err.h"
#include <string.h>
#include "list_networks.h"
#include "list_stations.h"
#include "probe_networks.h"
#include "fake_network.h"

static sniff_plugin * loadPlugin(char *name);

struct list plugins_list;

void initPlugins(FrameListener * listener)
{
	list_init(&plugins_list);

	list_push_back(&listener->activeCommands, &LIST_getlist()->elem);
	//list_push_back(&listener->activeCommands, &PROBE_getlist()->elem);
	//list_push_back(&listener->activeCommands, &FAKE_getlist()->elem);
	//list_push_back(&listener->activeCommands, &STATION_getlist()->elem);

	struct list_elem * e = list_begin(&listener->activeCommands);
	for(; e != list_end(&listener->activeCommands); e = list_next(e))
	{
		sniff_plugin * plugin = list_entry(e, sniff_plugin, elem);
		plugin->init(listener);
	}
}


sniff_plugin * findPlugin(char * name)
{
	struct list_elem * e = list_begin(&plugins_list);
	for(; e != list_end(&plugins_list); e = list_next(e))
	{
		sniff_plugin * plugin = list_entry(e, sniff_plugin, elem);
		if(strstr(name, plugin->name))
		{
			return plugin;
		}
	}
	return NULL;
}

void printplugins()
{
	fprintf(stderr, "Commands:\n");
	struct list_elem * e = list_begin(&plugins_list);
	for(; e != list_end(&plugins_list); e = list_next(e))
	{
		sniff_plugin * plugin = list_entry(e, sniff_plugin, elem);
		fprintf(stdout, "\t%s\n\t\t%s\n", plugin->name, plugin->description);
	}
}

void loadPlugins(char * directory)
{
	DIR * dir = opendir(directory);
	if (dir == NULL)
	{
		fprintf(stderr, "Directory %s does not exist\n", directory);
		return;
	}
	struct dirent * entry;
	while((entry = readdir(dir)) != NULL)
	{
		if(!strstr(entry->d_name, ".so"))
		{
			continue;
		}
		char buf[1025];
		snprintf(buf, 1025, "%s/%s", directory, entry->d_name);
		sniff_plugin * plug = loadPlugin(buf);
		if(plug);
			list_push_back(&plugins_list, &plug->elem);
	}

	closedir(dir);
}

static sniff_plugin * loadPlugin(char *name)
{
	void * dhandle = dlopen(name, RTLD_LAZY);
	if(!dhandle)
	{
		fprintf(stderr, "Could not load plugin.\n");
		return NULL;
	}
	sniff_plugin * plug = dlsym(dhandle, "plugin_module");
	if(plug == NULL){
		fprintf(stderr, "Plugin does not define correct module\n");
		dlclose(dhandle);
		return NULL;
	}
	return plug;
}