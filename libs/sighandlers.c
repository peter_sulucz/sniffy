#include "sighandlers.h"
#include <stdio.h>
#include "list.h"
#include "plugins.h"
#include "stdlib.h"

static FrameListener * listener;
static void addhandler(int sig, sighandler handle);
void signalblock(int signal);
void signalunblock(int signal);
static void __sig_mask(int signal, int block);
void HANDLEsigAlarm(int sig, siginfo_t * info, void * idk);

void initSighandlers(FrameListener * framelistener)
{
	listener = framelistener;
	addhandler(SIGALRM, HANDLEsigAlarm);
	alarm(1);
}

static void __sig_mask(int sig, int block)
{
	sigset_t mask, omask;
	sigemptyset(&mask);
	sigaddset(&mask, sig);
	if(sigprocmask(block, &mask, &omask) != 0)
		fprintf(stderr, "Sigblock error\n");
}

static void addhandler(int sig, sighandler handle)
{
	sigset_t mask;
	sigemptyset(&mask);
	struct sigaction act = {
		.sa_sigaction = handle,
		.sa_mask = mask,
		.sa_flags = SA_SIGINFO
	};

	if(sigaction(sig, &act, NULL) != 0)
		fprintf(stderr, "SIG HANDLE ERROR\n");
}


void HANDLEsigAlarm(int sig, siginfo_t * info, void * idk)
{
	signalblock(SIGALRM);
	struct list_elem * e = list_begin(&listener->activeCommands);
	for(; e != list_end(&listener->activeCommands); e = list_next(e))
	{
		sniff_plugin * plugin = list_entry(e, sniff_plugin, elem);
		if(plugin->tick)
			plugin->tick();
	}
	signalblock(SIGALRM);
	ualarm(200000, 200000);
}

void signalblock(int signal)
{
	__sig_mask(signal, SIG_BLOCK);
}
void signalunblock(int signal)
{
	__sig_mask(signal, SIG_UNBLOCK);
}