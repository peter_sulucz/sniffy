#ifndef ENCRYPTION_RC4_H
#define ENCRYPTION_RC4_H
#include <string.h>

#define SBOX_LENGTH 256

typedef struct {
	unsigned char key[SBOX_LENGTH];
	unsigned int sbox[SBOX_LENGTH];
} RC4;

RC4 * INIT_rc4(char * key);
void KILL_rc4(RC4 * rc4);

#endif