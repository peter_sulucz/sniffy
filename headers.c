#include "headers.h"
#include "libs/frames.h"
#include "libs/beacon.h"
#include "libs/threadpool.h"
#include "libs/list.h"
#include <stdio.h>

#include <linux/ip.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


static fh_wifi * parseRadioTapHeader(fh_frame * frame);
static void parseIPHeader(fh_frame * frame);
static void printWifiHeader(fh_wifi * wifi);

unsigned short parseFrame(FrameListener * listener, fh_frame * frame)
{
	unsigned short ret = 0;
	//fh_ether * eth = &frame->frame.ethernet_header;

	fh_wifi * wifi = parseRadioTapHeader(frame);

	ret |= wifi->framecontrol.type << 8;
	ret |= wifi->framecontrol.subtype;

	return ret;
}

static fh_wifi * parseRadioTapHeader(fh_frame * frame)
{
	fh_radiotap * radio = (fh_radiotap *) frame;
	//fprintf(stdout, "Radiotap flags: %u\n", radio->present);
	fh_wifi * wifi = (fh_wifi *)((char *)radio + radio->length);
	return wifi;
}


static void printWifiHeader(fh_wifi * wifi)
{
	//char type;
	switch(wifi->framecontrol.type)
	{
		case(0):
			//fprintf(stdout, "Management Frame ");
			switch(wifi->framecontrol.subtype)
			{
				case(8):
					//fprintf(stdout, "BEACON\n");
					//parseBeacon(wifi);
					break;
				case(11):
					//fprintf(stdout, "Authentication\n");
				break;
				case(12):
					//fprintf(stdout, "Deauthentication\n");
				break;
				default:
					//type = wifi->subtype;
					//fprintf(stdout, "TYPE %hhu\n", type);
				break;
			}
			//fprintf(stdout, "\n");
			break;
		case(1):
			//fprintf(stdout, "Control Frame\n");
			break;
		case(2):
			//fprintf(stdout, "Data Frame\n");
			/*switch(wifi->subtype)
			{
				case(0b0000):
					case_data:
					fprintf(stdout, "Data\n");
					break;

			}*/
			break;
		case(3):
			fprintf(stdout, "UH OH\n");
			break;
	}

	/*
	if(wifi->framecontrol.wep)
	{
		fprintf(stdout, "ENCRYPTED\n");
	}
	
	if(wifi->fromDS)
	{
		if(wifi->toDS)
		{
			fprintf(stdout, "END AP: ");
			printMAC(wifi->addr1);
			fprintf(stdout, "\n");
			fprintf(stdout, "Start AP: ");
			printMAC(wifi->addr2);
			fprintf(stdout, "\n");
			fprintf(stdout, "End Station: ");
			printMAC(wifi->addr3);
			fprintf(stdout, "\n");
			fprintf(stdout, "Start Station: ");
			printMAC(wifi->addr4);
			fprintf(stdout, "\n");
		}
		else
		{
			fprintf(stdout, "RA/DA: ");
			printMAC(wifi->addr1);
			fprintf(stdout, "\n");
			fprintf(stdout, "TA/BSSID: ");
			printMAC(wifi->addr2);
			fprintf(stdout, "\n");
			fprintf(stdout, "SA: ");
			printMAC(wifi->addr3);
			fprintf(stdout, "\n");
		}
	}
	else
	{
		if(wifi->toDS)
		{
			fprintf(stdout, "RA/BSSID: ");
			printMAC(wifi->addr1);
			fprintf(stdout, "\n");
			fprintf(stdout, "TA/SA: ");
			printMAC(wifi->addr2);
			fprintf(stdout, "\n");
			fprintf(stdout, "DA: ");
			printMAC(wifi->addr3);
			fprintf(stdout, "\n");
		}
		else
		{
			fprintf(stdout, "DA: ");
			printMAC(wifi->addr1);
			fprintf(stdout, "\n");
			fprintf(stdout, "SA: ");
			printMAC(wifi->addr2);
			fprintf(stdout, "\n");
			fprintf(stdout, "RA: ");
			printMAC(wifi->addr3);
			fprintf(stdout, "\n");
			fprintf(stdout, "TA: ");
			printMAC(wifi->addr4);
			fprintf(stdout, "\n");
		}
	}
	*/
}

static void parseIPHeader(fh_frame * frame)
{
	unsigned short ipheaderlen;
	struct  iphdr * iph = (struct iphdr *)(frame->buffer + sizeof(fh_ether));
	ipheaderlen = iph->ihl*4;

	struct sockaddr_in source = {0};
	struct sockaddr_in dest = {0};

	source.sin_addr.s_addr = iph->saddr;
	dest.sin_addr.s_addr = iph->daddr;
	/*
	//ip header
	fprintf(stdout , "\n");
    fprintf(stdout , "IP Header\n");
    fprintf(stdout , "   |-IP Version        : %d\n",(unsigned int)iph->version);
    fprintf(stdout , "   |-IP Header Length  : %d DWORDS or %d Bytes\n",(unsigned int)iph->ihl,((unsigned int)(iph->ihl))*4);
    fprintf(stdout , "   |-Type Of Service   : %d\n",(unsigned int)iph->tos);
    fprintf(stdout , "   |-IP Total Length   : %d  Bytes(Size of Packet)\n",ntohs(iph->tot_len));
    fprintf(stdout , "   |-Identification    : %d\n",ntohs(iph->id));
    //fprintf(logfile , "   |-Reserved ZERO Field   : %d\n",(unsigned int)iphdr->ip_reserved_zero);
    //fprintf(logfile , "   |-Dont Fragment Field   : %d\n",(unsigned int)iphdr->ip_dont_fragment);
    //fprintf(logfile , "   |-More Fragment Field   : %d\n",(unsigned int)iphdr->ip_more_fragment);
    fprintf(stdout , "   |-TTL      : %d\n",(unsigned int)iph->ttl);
    fprintf(stdout , "   |-Protocol : %d\n",(unsigned int)iph->protocol);
    fprintf(stdout , "   |-Checksum : %d\n",ntohs(iph->check));
    fprintf(stdout , "   |-Source IP        : %s\n",inet_ntoa(source.sin_addr));
    fprintf(stdout , "   |-Destination IP   : %s\n",inet_ntoa(dest.sin_addr));
    */
    fprintf(stdout, "%s -> %s\n", inet_ntoa(source.sin_addr), inet_ntoa(dest.sin_addr));

}