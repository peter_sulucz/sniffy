cmake_minimum_required(VERSION 2.8.4)
project(Network)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu99")

set(SOURCE_FILES
    lists/list.c
    lists/list.h
    threads/threadpool.c
    threads/threadpool.h
    types/frames.h
    err.c
    err.h
    headers.c
    headers.h
    main.c
    main.h)

include_directories(${CMAKE_SOURCE_DIR}/lists)
include_directories(${CMAKE_SOURCE_DIR}/threads)
include_directories(${CMAKE_SOURCE_DIR}/types)


add_executable(Network  ${SOURCE_FILES})