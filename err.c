#include "err.h"
#include <stdio.h>
#include <stdlib.h>

void error(char * text)
{
	fprintf(stderr, "[ERROR] %s\n", text);
	exit(1);
}

void warn(char * text)
{
	fprintf(stderr, "[WARN] %s\n", text);
}

void LOG(char * text)
{
	fprintf(stderr, "[LGO] %s\n", text);
}