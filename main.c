#include "listener.h"
#include "err.h"
#include <stdio.h>
#include <string.h>
#include "libs/plugins.h"
#include <stdlib.h>
#include "libs/datastructures.h"
#include "libs/command_line.h"
#include "libs/sighandlers.c"

int main(int argc, char * argv[])
{
	if(argc < 2)
		error("Specify interface name");
	init_datastructures();
	FrameListener * listener = INIT_Listener();
	initPlugins(listener);
	initSighandlers(listener);
	BeginListener(listener, argv[1]);
	char buffer[1024] = {0};
	while(true)
	{
		fprintf(stderr, "sniffy>>");
		interupt:
		fgets(buffer, 1024, stdin);
		if(buffer[0] == 0)
			goto interupt;
		cmd_line * cmd = parseline(buffer);
		if(strstr(buffer, "exit"))
			break;
		sniff_plugin * active = activePlugin(listener, cmd->prog);
		if(active != NULL)
		{
			active->verbose(cmd);
			goto end;
		}
		sniff_plugin * plugin = findPlugin(cmd->prog);
		if(plugin == NULL)
		{
			fprintf(stderr, "NOT FOUND\n");
			printplugins();
			goto end;
		}

		sniff_plugin * cpy = (sniff_plugin *)malloc(sizeof(sniff_plugin));
		memcpy(cpy, plugin, sizeof(sniff_plugin));
		addplugin(listener, cpy);
		end:
			killCmdLine(cmd);
		buffer[0] = 0;
	}	
	return 0;
}








