#include "listener.h"
#include "libs/threadpool.h"
#include "headers.h"
#include <stdio.h>
#include <asm/types.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <errno.h>
#include <string.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include "err.h"
#include "pthread.h"

static int getInterfaceIndex(int sockfd, char * interfacename);
static int createSocket();
static void socketConfig(FrameListener * listener);
static void listenerListen(struct thread_pool * pool, void * data);
static void processFrame(struct thread_pool * pool, void * data);

struct proc_frame_data
{
	FrameListener * listener;
	fh_frame * frame;
};

FrameListener * INIT_Listener()
{

	FrameListener * listener = (FrameListener *)malloc(sizeof(FrameListener));
	listener->threadpool = thread_pool_new(16);
	list_init(&listener->activeCommands);
	fprintf(stderr, "Welcome to sniffy\n");
	pthread_mutex_init(&listener->listmutex, (pthread_mutexattr_t *)NULL);
	return listener;
}

void KILL_LISTENER(FrameListener * listener)
{
	close(listener->socket);
}

static void processFrame(struct thread_pool * pool, void * data)
{

	struct proc_frame_data * pfd = (struct proc_frame_data *)data;
	struct list * active_list = &pfd->listener->activeCommands;
	
	pthread_mutex_lock(&pfd->listener->listmutex);
	if(list_empty(active_list))
	{
		pthread_mutex_unlock(&pfd->listener->listmutex);
		return;
	}

	short ftype = parseFrame(pfd->listener, pfd->frame);

	char type = ftype >> 8;
	char sub = ftype & 0xFF;

	struct list_elem * e = list_begin(active_list);
	for(; e != list_end(active_list); e = list_next(e))
	{
		sniff_plugin * plugin = list_entry(e, sniff_plugin, elem);
		if(plugin->process && plugin->filter && plugin->filter(type, sub))
		{
			pthread_mutex_unlock(&pfd->listener->listmutex);
			plugin->process((char *)pfd->frame, 11000);
			pthread_mutex_lock(&pfd->listener->listmutex);
		}
			
	}
	pthread_mutex_unlock(&pfd->listener->listmutex);
	// clean up
	KILL_Frame(pfd->frame);
	free(pfd);
}


void addplugin(FrameListener * listener, sniff_plugin * plugin)
{
	plugin->init(listener);
	list_push_front(&listener->activeCommands, &plugin->elem);
}

static void listenerListen(struct thread_pool * pool, void * data)
{
	FrameListener * listener = (FrameListener *)data;
	while(listener->running)
	{
		fh_frame * frame = INIT_Frame();
		int len = recv(listener->socket, frame, 11000, 0);
		struct proc_frame_data * fdata = (struct proc_frame_data *)malloc(sizeof(struct proc_frame_data));
		fdata->listener = listener;
		fdata->frame = frame;
		thread_pool_submit(pool, (fork_join_task_t)processFrame, fdata);
	}
}

int BeginListener(FrameListener * listener, char * networkDevice)
{
	listener->running = 1;
	listener->socket = createSocket();
	listener->deviceName = networkDevice;
	listener->networkDevice = getInterfaceIndex(listener->socket, networkDevice);
	socketConfig(listener);
	thread_pool_submit(listener->threadpool, (fork_join_task_t)listenerListen, listener);
}

int createSocket()
{
	int fd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if(fd < 0)
	{
		error(strerror(errno));
	}
	return fd;
}

static int getInterfaceIndex(int sockfd, char * interfacename)
{
	struct ifreq ifr;
	size_t name_len = strlen(interfacename);
	if(name_len < sizeof(ifr.ifr_name))
	{
		memcpy(ifr.ifr_name, interfacename, name_len);
		ifr.ifr_name[name_len] = 0;
	}
	else
	{
		error("Name too long");
	}
	if(ioctl(sockfd, SIOCGIFINDEX, &ifr) < 0){
		error(strerror(errno));
	}

	return ifr.ifr_ifindex;
}

void socketConfig(FrameListener * listener)
{
	struct sockaddr_ll sll = {0};
	sll.sll_family = PF_PACKET;
	sll.sll_ifindex = listener->networkDevice;
	sll.sll_protocol = htons(ETH_P_ALL);
	bind(listener->socket, (struct sockaddr *) &sll, sizeof(sll));

	struct packet_mreq mreq;
	memset(&mreq, 0, sizeof(mreq));
	mreq.mr_ifindex = listener->networkDevice;
	mreq.mr_type = PACKET_MR_PROMISC;
	setsockopt(listener->socket, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
}

sniff_plugin * activePlugin(FrameListener * listener, char * name)
{
	sniff_plugin * ret = NULL;
	pthread_mutex_lock(&listener->listmutex);
	if(list_empty(&listener->activeCommands))
	{
		pthread_mutex_unlock(&listener->listmutex);
		return NULL;
	}

	struct list_elem * e = list_begin(&listener->activeCommands);
	for(; e != list_end(&listener->activeCommands); e = list_next(e))
	{
		sniff_plugin * plugin = list_entry(e, sniff_plugin, elem);
		if(strcmp(plugin->name, name) == 0)
		{
			ret = plugin;
			goto end;
		}
	}
	end:
	pthread_mutex_unlock(&listener->listmutex);
	return ret;
}