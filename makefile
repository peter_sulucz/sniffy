CC=gcc

CFLAGS= -ggdb3 -c -std=gnu99
LFLAGS= -pthread -ldl $(LLIBS)
LLIBS= -Llibs
SRC=main.c err.c headers.c listener.c
OBJ=$(SRC:.c=.o)
EXECUTABLE=sniffy


all: dirs $(SRC) $(EXECUTABLE)

dirs:
	cd libs; make

dirsclean:
	cd libs; make clean

$(EXECUTABLE): $(OBJ)
	$(CC) $(LFLAGS) $(OBJ) -o $@ -lsniff

.c.o:
	$(CC) $(CFLAGS) $(LFLAGS)  $< -o $@

clean: dirsclean
	rm -rf *.o $(EXECUTABLE)
