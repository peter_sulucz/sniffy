#ifndef LISTENER_H
#define LISTENER_H
#include "libs/frames.h"
#include "libs/list.h"
#include "libs/plugins.h"

FrameListener * INIT_Listener();

void KILL_LISTENER(FrameListener * listener);

int BeginListener(FrameListener * listener, char * networkDevice);

void addplugin(FrameListener * listener, sniff_plugin * plugin);

sniff_plugin * activePlugin(FrameListener * listener, char * name);
#endif